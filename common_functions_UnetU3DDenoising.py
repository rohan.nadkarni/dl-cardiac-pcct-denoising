#Written by: Rohan Nadkarni

#This file contains modified versions of the common functions for UnetU Denoising
#that operate on 3D patches rather than 2D slices

import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, TensorDataset, DataLoader
import torchvision
import torchvision.transforms as T
import torchvision.transforms.functional as TF
import nibabel as nib
from collections import OrderedDict
import matplotlib.pyplot as plt
import time
from common_functions_UnetUDenoising import make1DSeparableGaussianKernelsFor3DFilter

class CustomDataset3D(Dataset):
    def __init__(self,U0_stack,Ug_stack,iter_stack,Uhat_iter_stack,set_stack,Minv_stack,S_stack,Vh_stack,Winv_stack,Md_stack,flips):
        self.U0_stack = U0_stack
        self.Ug_stack = Ug_stack
        self.iter_stack = iter_stack
        self.Uhat_iter_stack = Uhat_iter_stack        
        self.set_stack = set_stack
        self.Minv_stack = Minv_stack
        self.S_stack = S_stack
        self.Vh_stack = Vh_stack
        self.Winv_stack = Winv_stack
        self.Md_stack = Md_stack
        self.flips = flips

    def transform(self,U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i):
        if random.random() > 0.5:
            U0_i = TF.hflip(U0_i)
            Ug_i = TF.hflip(Ug_i)
            iter_i = TF.hflip(iter_i)
            Uhat_iter_i = TF.hflip(Uhat_iter_i)
            
        if random.random() > 0.5:
            U0_i = TF.vflip(U0_i)
            Ug_i = TF.vflip(Ug_i)
            iter_i = TF.vflip(iter_i)
            Uhat_iter_i = TF.vflip(Uhat_iter_i)

        #5/8/23: Incorporating Darin's suggestion of SVD sign flip as a
        #form of data augmentation
        if random.random() > 0.5:
            U0_i = -1*U0_i
            Ug_i = -1*Ug_i
            Uhat_iter_i = -1*Uhat_iter_i
            Vh_i = -1*Vh_i

        return U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i



    def __getitem__(self,idx):
        U0_i = self.U0_stack[idx,:,:,:,:]
        Ug_i = self.Ug_stack[idx,:,:,:,:]
        iter_i = self.iter_stack[idx,:,:,:,:]
        Uhat_iter_i = self.Uhat_iter_stack[idx,:,:,:,:]
        set_i = self.set_stack[idx]
        Minv_i = self.Minv_stack[int(set_i.item()),:,:]
        S_i = self.S_stack[int(set_i.item()),:,:]
        Vh_i = self.Vh_stack[int(set_i.item()),:,:]
        Winv_i = self.Winv_stack[int(set_i.item()),:,:]
        Md_i = self.Md_stack[int(set_i.item()),:,:]
        
        if self.flips:
            U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i = self.transform(U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i)

        return U0_i,Ug_i,iter_i,Uhat_iter_i,set_i,Minv_i,S_i,Vh_i,Winv_i,Md_i

    def __len__(self):
        return self.U0_stack.size(dim=0)

class CustomTestDataset3D(Dataset):
    def __init__(self,U0_stack,Ug_stack,set_stack,Minv_stack,S_stack,Vh_stack,Winv_stack):
        self.U0_stack = U0_stack
        self.Ug_stack = Ug_stack
        self.set_stack = set_stack
        self.Minv_stack = Minv_stack
        self.S_stack = S_stack
        self.Vh_stack = Vh_stack
        self.Winv_stack = Winv_stack

    def __getitem__(self,idx):
        U0_i = self.U0_stack[idx,:,:,:,:]
        Ug_i = self.Ug_stack[idx,:,:,:,:]
        set_i = self.set_stack[idx]
        Minv_i = self.Minv_stack[int(set_i.item()),:,:]
        S_i = self.S_stack[int(set_i.item()),:,:]
        Vh_i = self.Vh_stack[int(set_i.item()),:,:]
        Winv_i = self.Winv_stack[int(set_i.item()),:,:]
        
        return U0_i,Ug_i,set_i,Minv_i,S_i,Vh_i,Winv_i

    def __len__(self):
        return self.U0_stack.size(dim=0)


#4/29/2024: Version that loads the saved .pt files which each contain half of the shuffled training set
#Since the .pt files have shuffled order, set shuffling to False in Data Loader
class CustomDataset3D_V2(Dataset):
    def __init__(self,filenames,Md_filename,nb1,nb2,flips):
        self.filenames = filenames #names of the two files that each contain half of the shuffled training set
        self.nb1 = nb1 #number of elements in batch dimension of first file
        self.nb2 = nb2 #number of elements in batch dimension of second file
        self.nbtot = self.nb1 + self.nb2
        self.flips = flips
        self.Md_all = torch.load(Md_filename) #Tensor containing six 3x4 sensitivity matrices, one for each training set

        self.U0_all = None
        self.Ug_all = None
        self.set_all = None
        self.Minv_all = None
        self.S_all = None
        self.Vh_all = None
        self.Winv_all = None
        self.pcd_iter_all = None
        self.Uhat_iter_all = None
        
    def transform(self,U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i):
        if random.random() > 0.5:
            U0_i = TF.hflip(U0_i)
            Ug_i = TF.hflip(Ug_i)
            iter_i = TF.hflip(iter_i)
            Uhat_iter_i = TF.hflip(Uhat_iter_i)
            
        if random.random() > 0.5:
            U0_i = TF.vflip(U0_i)
            Ug_i = TF.vflip(Ug_i)
            iter_i = TF.vflip(iter_i)
            Uhat_iter_i = TF.vflip(Uhat_iter_i)

        #5/8/23: Incorporating Darin's suggestion of SVD sign flip as a
        #form of data augmentation
        if random.random() > 0.5:
            U0_i = -1*U0_i
            Ug_i = -1*Ug_i
            Uhat_iter_i = -1*Uhat_iter_i
            Vh_i = -1*Vh_i

        return U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i

    def __getitem__(self,idx):
        if idx == 0:
            self.U0_all, self.Ug_all, self.set_all, self.Minv_all, self.S_all, self.Vh_all, self.Winv_all, self.pcd_iter_all, self.Uhat_iter_all = torch.load(self.filenames[0])
            print("Done loading first half of set")
        if idx == self.nb1:
            self.U0_all, self.Ug_all, self.set_all, self.Minv_all, self.S_all, self.Vh_all, self.Winv_all, self.pcd_iter_all, self.Uhat_iter_all = torch.load(self.filenames[1])
            print("Done loading second half of set")
        #to make sure we extract correct indices after we load the second file
        if idx >= self.nb1:
            idx2 = idx - self.nb1
        else:
            idx2 = idx
            
        U0_i = self.U0_all[idx2,:,:,:,:]
        Ug_i = self.Ug_all[idx2,:,:,:,:]
        iter_i = self.pcd_iter_all[idx2,:,:,:,:]
        Uhat_iter_i = self.Uhat_iter_all[idx2,:,:,:,:]
        set_i = self.set_all[idx2]
        Minv_i = self.Minv_all[int(set_i.item()),:,:]
        S_i = self.S_all[int(set_i.item()),:,:]
        Vh_i = self.Vh_all[int(set_i.item()),:,:]
        Winv_i = self.Winv_all[int(set_i.item()),:,:]
        Md_i = self.Md_all[int(set_i.item()),:,:]
        
        if self.flips:
            U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i = self.transform(U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i)

        return U0_i,Ug_i,iter_i,Uhat_iter_i,set_i,Minv_i,S_i,Vh_i,Winv_i,Md_i

    def __len__(self):
        return self.nbtot



def recoverPCDImagesFromUhat_3D(Uhat2, Ugb2, setsb2, Minvb2, Sb2, Vhb2, Winvb2):
    #Original dimension order is nb, ne, nz, nx, ny
    #Want to convert to nb, nx, ny, nz, ne
    Uhat2 = torch.permute(Uhat2,(0,3,4,2,1))
    Ugb2 = torch.permute(Ugb2,(0,3,4,2,1))

    nb = Uhat2.size(dim=0)
    nx = Uhat2.size(dim=1)
    ny = Uhat2.size(dim=2)
    nz = Uhat2.size(dim=3)
    ne = Uhat2.size(dim=4)

    Uhat_vects = torch.reshape(Uhat2,(nb,nx*ny*nz,ne))
    Ugb_vects = torch.reshape(Ugb2,(nb,nx*ny*nz,ne))

    Uhp_vects_rec = torch.bmm(Uhat_vects,torch.inverse(Minvb2))
    U_vects_rec = Uhp_vects_rec + Ugb_vects
    X_vects_rec = torch.bmm(torch.bmm(U_vects_rec,Sb2),Vhb2)
    X_vects_rec = torch.bmm(X_vects_rec,torch.inverse(Winvb2))
    X_rec = torch.reshape(X_vects_rec,(nb,nx,ny,nz,ne))
    X_rec = torch.permute(X_rec,(0,4,3,1,2)) #shape needed for loss calculation (nb,ne,nz,nx,ny)

    return X_rec

def recoverDecompFromX_3D(X,Md):
    #Original dimension order is nb, ne, nz, nx, ny
    #Want to convert to nb, nx, ny, nz, ne
    X = torch.permute(X,(0,3,4,2,1))

    nb = X.size(dim=0)
    nx = X.size(dim=1)
    ny = X.size(dim=2)
    nz = X.size(dim=3)
    ne = X.size(dim=4)
    nm = Md.size(dim=1)

    X_vects = torch.reshape(X,(nb,nx*ny*nz,ne))
    C_vects = torch.bmm(X_vects,torch.linalg.pinv(Md))
    C = torch.reshape(C_vects,(nb,nx,ny,nz,nm))
    C = torch.permute(C,(0,4,3,1,2)) #shape needed for loss calculation (nb,nm,nz,nx,ny)

    return C


def MSEofXandUandCPlusMSEofXBlurred_3D(Uhat1, Ugb1, X_trueb1, Uhat_iterb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1, Mdb1, lambd1, lambd2, lambd3, device):
    X_pred1 = recoverPCDImagesFromUhat_3D(Uhat1, Ugb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1)

    C_pred1 = recoverDecompFromX_3D(X_pred1, Mdb1)
    C_trueb1 = recoverDecompFromX_3D(X_trueb1, Mdb1)

    msex_term = torch.mean(torch.square(X_pred1-X_trueb1))

    mseu_term = lambd1*torch.mean(torch.square(Uhat1-Uhat_iterb1))
    
    msec_term = lambd2*torch.mean(torch.square(C_pred1-C_trueb1))

    #X_blur has to be computed in 3D now
    fwhm = 5
    kernel_size = 19
    Gx,Gy,Gz = make1DSeparableGaussianKernelsFor3DFilter(fwhm,kernel_size)
    Gx,Gy,Gz = Gx.to(device),Gy.to(device),Gz.to(device)
    
    X_pred1_blur = torch.zeros(X_pred1.size()).double().to(device)
    X_trueb1_blur = torch.zeros(X_trueb1.size()).double().to(device)
    ne = X_pred1.size(dim=1)
    for ee in range(ne):
        X_pred1_e = X_pred1[:,ee,:,:]
        X_pred1_e = X_pred1_e.to(device)
        X_pred1_e = torch.unsqueeze(X_pred1_e,1)
        X_pred1_blur[:,ee:ee+1,:,:] = F.conv3d(F.conv3d(F.conv3d(X_pred1_e,Gz,padding='same'),Gy,padding='same'),Gx,padding='same')

        X_trueb1_e = X_trueb1[:,ee,:,:]
        X_trueb1_e = X_trueb1_e.to(device)
        X_trueb1_e = torch.unsqueeze(X_trueb1_e,1)
        X_trueb1_blur[:,ee:ee+1,:,:] = F.conv3d(F.conv3d(F.conv3d(X_trueb1_e,Gz,padding='same'),Gy,padding='same'),Gx,padding='same')
    
    msex_blur_term = lambd3*torch.mean(torch.square(X_pred1_blur-X_trueb1_blur))

    loss = msex_term + mseu_term + msec_term + msex_blur_term
    return loss, msex_term, mseu_term, msec_term, msex_blur_term

