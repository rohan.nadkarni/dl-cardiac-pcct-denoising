#Written by: Rohan Nadkarni

#This file contains functions for our transformation of the wFBP to
#normalized, high-pass filtered left singular vectors (U),
#corresponding custom datasets, and our custom loss function.

import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, TensorDataset, DataLoader
import torchvision
import torchvision.transforms as T
import torchvision.transforms.functional as TF
import nibabel as nib
from collections import OrderedDict
import matplotlib.pyplot as plt
import time

class CustomDataset(Dataset):
    def __init__(self,U0_stack,Ug_stack,iter_stack,Uhat_iter_stack,set_stack,Minv_stack,S_stack,Vh_stack,Winv_stack,Md_stack,flips):
        self.U0_stack = U0_stack
        self.Ug_stack = Ug_stack
        self.iter_stack = iter_stack
        self.Uhat_iter_stack = Uhat_iter_stack        
        self.set_stack = set_stack
        self.Minv_stack = Minv_stack
        self.S_stack = S_stack
        self.Vh_stack = Vh_stack
        self.Winv_stack = Winv_stack
        self.Md_stack = Md_stack
        self.flips = flips

    def transform(self,U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i):
        if random.random() > 0.5:
            U0_i = TF.hflip(U0_i)
            Ug_i = TF.hflip(Ug_i)
            iter_i = TF.hflip(iter_i)
            Uhat_iter_i = TF.hflip(Uhat_iter_i)
            
        if random.random() > 0.5:
            U0_i = TF.vflip(U0_i)
            Ug_i = TF.vflip(Ug_i)
            iter_i = TF.vflip(iter_i)
            Uhat_iter_i = TF.vflip(Uhat_iter_i)

        #5/8/23: Incorporating Darin's suggestion of SVD sign flip as a
        #form of data augmentation
        if random.random() > 0.5:
            U0_i = -1*U0_i
            Ug_i = -1*Ug_i
            Uhat_iter_i = -1*Uhat_iter_i
            Vh_i = -1*Vh_i

        return U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i



    def __getitem__(self,idx):
        U0_i = self.U0_stack[idx,:,:,:]
        Ug_i = self.Ug_stack[idx,:,:,:]
        iter_i = self.iter_stack[idx,:,:,:]
        Uhat_iter_i = self.Uhat_iter_stack[idx,:,:,:]
        set_i = self.set_stack[idx]
        Minv_i = self.Minv_stack[int(set_i.item()),:,:]
        S_i = self.S_stack[int(set_i.item()),:,:]
        Vh_i = self.Vh_stack[int(set_i.item()),:,:]
        Winv_i = self.Winv_stack[int(set_i.item()),:,:]
        Md_i = self.Md_stack[int(set_i.item()),:,:]
        
        if self.flips:
            U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i = self.transform(U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i)

        return U0_i,Ug_i,iter_i,Uhat_iter_i,set_i,Minv_i,S_i,Vh_i,Winv_i,Md_i

    def __len__(self):
        return self.U0_stack.size(dim=0)

class CustomTestDataset(Dataset):
    def __init__(self,U0_stack,Ug_stack,set_stack,Minv_stack,S_stack,Vh_stack,Winv_stack):
        self.U0_stack = U0_stack
        self.Ug_stack = Ug_stack
        self.set_stack = set_stack
        self.Minv_stack = Minv_stack
        self.S_stack = S_stack
        self.Vh_stack = Vh_stack
        self.Winv_stack = Winv_stack

    def __getitem__(self,idx):
        U0_i = self.U0_stack[idx,:,:,:]
        Ug_i = self.Ug_stack[idx,:,:,:]
        set_i = self.set_stack[idx]
        Minv_i = self.Minv_stack[int(set_i.item()),:,:]
        S_i = self.S_stack[int(set_i.item()),:,:]
        Vh_i = self.Vh_stack[int(set_i.item()),:,:]
        Winv_i = self.Winv_stack[int(set_i.item()),:,:]

        return U0_i,Ug_i,set_i,Minv_i,S_i,Vh_i,Winv_i

    def __len__(self):
        return self.U0_stack.size(dim=0)

#4/29/2024: Version that loads the saved .pt files which each contain half of the shuffled training set
#Since the .pt files have shuffled order, set shuffling to False in Data Loader
class CustomDataset2D_V2(Dataset):
    def __init__(self,filenames,Md_filename,nb1,nb2,flips):
        self.filenames = filenames #names of the two files that each contain half of the shuffled training set
        self.nb1 = nb1 #number of elements in batch dimension of first file
        self.nb2 = nb2 #number of elements in batch dimension of second file
        self.nbtot = self.nb1 + self.nb2
        self.flips = flips
        self.Md_all = torch.load(Md_filename) #Tensor containing six 3x4 sensitivity matrices, one for each training set

        self.U0_all = None
        self.Ug_all = None
        self.set_all = None
        self.Minv_all = None
        self.S_all = None
        self.Vh_all = None
        self.Winv_all = None
        self.pcd_iter_all = None
        self.Uhat_iter_all = None
        
    def transform(self,U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i):
        if random.random() > 0.5:
            U0_i = TF.hflip(U0_i)
            Ug_i = TF.hflip(Ug_i)
            iter_i = TF.hflip(iter_i)
            Uhat_iter_i = TF.hflip(Uhat_iter_i)
            
        if random.random() > 0.5:
            U0_i = TF.vflip(U0_i)
            Ug_i = TF.vflip(Ug_i)
            iter_i = TF.vflip(iter_i)
            Uhat_iter_i = TF.vflip(Uhat_iter_i)

        #5/8/23: Incorporating Darin's suggestion of SVD sign flip as a
        #form of data augmentation
        if random.random() > 0.5:
            U0_i = -1*U0_i
            Ug_i = -1*Ug_i
            Uhat_iter_i = -1*Uhat_iter_i
            Vh_i = -1*Vh_i

        return U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i

    def __getitem__(self,idx):
        if idx == 0:
            self.U0_all, self.Ug_all, self.set_all, self.Minv_all, self.S_all, self.Vh_all, self.Winv_all, self.pcd_iter_all, self.Uhat_iter_all = torch.load(self.filenames[0])
            print("Done loading first half of set")
        if idx == self.nb1:
            self.U0_all, self.Ug_all, self.set_all, self.Minv_all, self.S_all, self.Vh_all, self.Winv_all, self.pcd_iter_all, self.Uhat_iter_all = torch.load(self.filenames[1])
            print("Done loading second half of set")
            
        #to make sure we extract correct indices after we load the second file
        if idx >= self.nb1:
            idx2 = idx - self.nb1
        else:
            idx2 = idx

        #5/8/2024: Change to 4D indexing
        U0_i = self.U0_all[idx2,:,:,:].float()
        Ug_i = self.Ug_all[idx2,:,:,:].float()
        iter_i = self.pcd_iter_all[idx2,:,:,:].float()
        Uhat_iter_i = self.Uhat_iter_all[idx2,:,:,:].float()
        #Need to pad to axial slice size 448 x 448 for SUNet
        pad = T.Pad(24)
        U0_i = pad(U0_i)
        Ug_i = pad(Ug_i)
        iter_i = pad(iter_i)
        Uhat_iter_i = pad(Uhat_iter_i)
        set_i = self.set_all[idx2]
        Minv_i = self.Minv_all[int(set_i.item()),:,:].float()
        S_i = self.S_all[int(set_i.item()),:,:].float()
        Vh_i = self.Vh_all[int(set_i.item()),:,:].float()
        Winv_i = self.Winv_all[int(set_i.item()),:,:].float()
        Md_i = self.Md_all[int(set_i.item()),:,:].float()
        
        if self.flips:
            U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i = self.transform(U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i)

        return U0_i,Ug_i,iter_i,Uhat_iter_i,set_i,Minv_i,S_i,Vh_i,Winv_i,Md_i

    def __len__(self):
        return self.nbtot



def create_circular_mask(h, w, center=None, radius=None):

    if center is None: # use the middle of the image
        center = (int(w/2), int(h/2))
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask

def make1DSeparableGaussianKernelsFor3DFilter(fwhm,kernel_size):
    twoT = torch.Tensor([[2]])
    sig = torch.Tensor([[fwhm]])/(twoT*torch.sqrt(twoT*torch.log(twoT)))
    rad = int((kernel_size-1)/2)
    dist = torch.arange(-rad,1+rad,1)
    G = torch.exp(-torch.square(dist)/(twoT*sig**2))
    G = G/torch.sum(G)
    Gz = torch.reshape(G,(1,1,kernel_size,1,1)).double()
    Gy = torch.transpose(Gz,2,3)
    Gx = torch.transpose(Gz,2,4)
    return Gx,Gy,Gz

def make1DSeparableGaussianKernelsFor2DFilter(fwhm,kernel_size,device):
    twoT = torch.Tensor([[2]])
    sig = torch.Tensor([[fwhm]])/(twoT*torch.sqrt(twoT*torch.log(twoT)))
    rad = int((kernel_size-1)/2)
    dist = torch.arange(-rad,1+rad,1)
    G = torch.exp(-torch.square(dist)/(twoT*sig**2))
    G = G/torch.sum(G)
    #5/9/2024: Change datatype from double to float for SUNet
    Gy = torch.reshape(G,(1,1,kernel_size,1)).float().to(device)
    Gx = torch.transpose(Gy,2,3)
    return Gx,Gy


def makeNormalizedHPSingularVector(X):
    nx = X.size(dim=0)
    ny = X.size(dim=1)
    nz = X.size(dim=2)
    ne = X.size(dim=3)

    rmask = create_circular_mask(int(nx),int(ny))
    rmask = torch.Tensor(rmask)
    rmask = torch.reshape(rmask,(1,1,nx,ny))
    rmask = rmask.double() #.to(device)
    
    filt1 = torch.Tensor([ [1], [-1] ]).double() #.to(device)
    filt1 = torch.reshape(filt1,(1,1,2,1))
    filt2 = torch.transpose(filt1,2,3)

    Winv = torch.zeros(4,4)
    Winv = Winv.double() #.to(device)

    sigma = torch.zeros(ne,1)
    for e in range(ne):
        X_e = X[:,:,int(nz/2),e]
        X_e = torch.reshape(X_e,(1,1,nx,ny))
        HP = 0.5*F.conv2d(F.conv2d(X_e,filt1,padding='same'),filt2,padding='same')
        HP_roi = HP*rmask
        HP_roi = torch.reshape(HP_roi,(-1,))
        HP_roi = HP_roi[HP_roi.nonzero()]
        sigma[e] = torch.median(torch.abs(HP_roi))/0.6745
        Winv[e,e] = 1/(torch.square(sigma[e]))

    print("Calculated noise variance weights")
    X = torch.reshape(X,(nx*ny*nz,ne))

    U,S,Vh = np.linalg.svd((torch.matmul(X,Winv)),full_matrices=False)
    U,S,Vh = torch.Tensor(U).double(),torch.Tensor(S).double(),torch.Tensor(Vh).double()
    print("Completed noise variance weighted SVD")
    S = torch.diag(S)
    U = torch.reshape(U,(nx,ny,nz,ne))
    U = torch.permute(U,(3,2,0,1))
    U = torch.unsqueeze(U,0)

    fwhm = 10 #2 #
    kernel_size = 37 #5 #
    Gx,Gy,Gz = make1DSeparableGaussianKernelsFor3DFilter(fwhm,kernel_size)
##    Gx,Gy,Gz = Gx.to(device),Gy.to(device),Gz.to(device)

    Ug = torch.zeros(U.size()) #.to(device)
    for ee in range(ne):
        U_e = U[:,ee,:,:,:]
        U_e = torch.unsqueeze(U_e,1)
        Ug[:,ee,:,:,:] = F.conv3d(F.conv3d(F.conv3d(U_e,Gz,padding='same'),Gy,padding='same'),Gx,padding='same')

    Uhp = U-Ug
    print("Completed high pass filtering")
    Uhp = torch.permute(torch.squeeze(Uhp),(2,3,1,0))

    Uhp_vects = torch.reshape(Uhp,(nx*ny*nz,ne))
##    Minv = torch.diag( torch.reciprocal( torch.median( torch.abs(Uhp_vects),0 ).values ) )
    Minv = torch.diag(torch.reciprocal(torch.Tensor(np.nanpercentile(torch.abs(Uhp_vects),99.95,axis=0)).double())) #.to(device)
    U0_vects = torch.matmul(Uhp_vects,Minv)
    print("Completed absolute 99.95 percentile normalization")
    U0 = torch.reshape(U0_vects,(nx,ny,nz,ne))
    return U0, Minv, Ug, S, Vh, Winv

def recoverPCDImagesFromUhat(Uhat2, Ugb2, setsb2, Minvb2, Sb2, Vhb2, Winvb2):
##    Uhat2 = -1*Uhat2 #RN 4/5/23: Have to reverse the sign flip you incorporated to give positive skew
    Uhat2 = torch.permute(Uhat2,(0,2,3,1))
    Ugb2 = torch.permute(Ugb2,(0,2,3,1))

    nz = Uhat2.size(dim=0)
    nx = Uhat2.size(dim=1)
    ny = Uhat2.size(dim=2)
    ne = Uhat2.size(dim=3)

    Uhat_vects = torch.reshape(Uhat2,(nz,nx*ny,ne))
    Ugb_vects = torch.reshape(Ugb2,(nz,nx*ny,ne))

    Uhp_vects_rec = torch.bmm(Uhat_vects,torch.inverse(Minvb2))
    U_vects_rec = Uhp_vects_rec + Ugb_vects
    X_vects_rec = torch.bmm(torch.bmm(U_vects_rec,Sb2),Vhb2)
    X_vects_rec = torch.bmm(X_vects_rec,torch.inverse(Winvb2))
    X_rec = torch.reshape(X_vects_rec,(nz,nx,ny,ne))
    X_rec = torch.permute(X_rec,(0,3,1,2)) #shape needed for loss calculation

    return X_rec

def makeUhatLabelFromFDKMatrices(pcd_iter,Minv,Ug,S,Vh,Winv):
    pcd_iter = torch.permute(pcd_iter,(2,3,0,1))
    Ug = torch.permute(Ug,(2,3,0,1))
    nx = pcd_iter.size(dim=0)
    ny = pcd_iter.size(dim=1)
    nz = pcd_iter.size(dim=2)
    ne = pcd_iter.size(dim=3)
    pcd_iter_vects = torch.reshape(pcd_iter,(nx*ny*nz,ne))
    Ug_vects = torch.reshape(Ug,(nx*ny*nz,ne))
    X_w_iter_vects = torch.matmul(pcd_iter_vects,Winv)
    U_iter_vects = torch.matmul(torch.matmul(X_w_iter_vects,torch.inverse(Vh)),torch.inverse(S))
    Uhp_iter_vects = U_iter_vects - Ug_vects
    Uhat_iter_vects = torch.matmul(Uhp_iter_vects,Minv)
    Uhat_iter = torch.reshape(Uhat_iter_vects,(nx,ny,nz,ne))
    Uhat_iter = torch.permute(Uhat_iter,(2,3,0,1))
    return Uhat_iter

def recoverDecompFromX(X,Md):
    X = torch.permute(X,(0,2,3,1))

    nz = X.size(dim=0)
    nx = X.size(dim=1)
    ny = X.size(dim=2)
    ne = X.size(dim=3)
    nm = Md.size(dim=1)

    X_vects = torch.reshape(X,(nz,nx*ny,ne))
    C_vects = torch.bmm(X_vects,torch.linalg.pinv(Md))
    C = torch.reshape(C_vects,(nz,nx,ny,nm))
    C = torch.permute(C,(0,3,1,2))

    return C

def loadCardiacSet(filename,crop_start,crop_end,crop):
    pcd_set = nib.load(filename)
    pcd_set = pcd_set.get_fdata()
    pcd_set = pcd_set.astype('float64')
    pcd_set = torch.Tensor(pcd_set).double()

    #Add crop because even though cardiac wFBP sets don't have edge of volume artifacts, their iterative RSKR sets have these artifacts.
    #Hopefully this means that cropping is not necessary in the test set.
    if crop:
        pcd_set = pcd_set[:,:,crop_start:crop_end,:,:]
        
    #unfold time dimension onto the z-dimension
    pcd_set = torch.reshape(pcd_set,(pcd_set.size(dim=0),pcd_set.size(dim=1),pcd_set.size(dim=2)*pcd_set.size(dim=3),pcd_set.size(dim=4)))
    pcd_set = pcd_set*1000
    return pcd_set

def MSEofXandUandCPlusMSEofXBlurred(Uhat1, Ugb1, X_trueb1, Uhat_iterb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1, Mdb1, lambd1, lambd2, lambd3, device):

    X_pred1 = recoverPCDImagesFromUhat(Uhat1, Ugb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1)

    C_pred1 = recoverDecompFromX(X_pred1, Mdb1)
    C_trueb1 = recoverDecompFromX(X_trueb1, Mdb1)

    msex_term = torch.mean(torch.square(X_pred1-X_trueb1))

    mseu_term = lambd1*torch.mean(torch.square(Uhat1-Uhat_iterb1))
    
    msec_term = lambd2*torch.mean(torch.square(C_pred1-C_trueb1))

    fwhm = 5
    kernel_size = 19
    Gx,Gy = make1DSeparableGaussianKernelsFor2DFilter(fwhm,kernel_size,device)
    X_pred1_blur = torch.zeros(X_pred1.size()).double().to(device)
    X_trueb1_blur = torch.zeros(X_trueb1.size()).double().to(device)
    ne = X_pred1.size(dim=1)
    for ee in range(ne):
        X_pred1_e = X_pred1[:,ee,:,:]
        X_pred1_e = X_pred1_e.to(device)
        X_pred1_e = torch.unsqueeze(X_pred1_e,1)
        X_pred1_blur[:,ee:ee+1,:,:] = F.conv2d(F.conv2d(X_pred1_e,Gx,padding='same'),Gy,padding='same')

        X_trueb1_e = X_trueb1[:,ee,:,:]
        X_trueb1_e = X_trueb1_e.to(device)
        X_trueb1_e = torch.unsqueeze(X_trueb1_e,1)
        X_trueb1_blur[:,ee:ee+1,:,:] = F.conv2d(F.conv2d(X_trueb1_e,Gx,padding='same'),Gy,padding='same')
    
    msex_blur_term = lambd3*torch.mean(torch.square(X_pred1_blur-X_trueb1_blur))

    loss = msex_term + mseu_term + msec_term + msex_blur_term
    return loss, msex_term, mseu_term, msec_term, msex_blur_term
