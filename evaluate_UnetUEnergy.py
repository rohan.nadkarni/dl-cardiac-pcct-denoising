#Written by: Rohan Nadkarni

#This code shows an example of how to use the trained UnetU Energy model to denoise a
#5D (3D + energy + time) cardiac PCCT wFBP reconstruction of a mouse that was held out for testing.
#For more information about this data, see Section 2.2 of our manuscript. Our training procedures
#are described in Section 2.5 of our manuscript.

import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, TensorDataset, DataLoader
from torch.utils.tensorboard import SummaryWriter
import torchvision
import torchvision.transforms as T
import torchvision.transforms.functional as TF
import nibabel as nib
import time
from common_functions_UnetUDenoising import CustomDataset, loadCardiacSet, makeNormalizedHPSingularVector, create_circular_mask, make1DSeparableGaussianKernelsFor3DFilter, makeUhatLabelFromFDKMatrices, recoverPCDImagesFromUhat  

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
batch_size = 8

class UNet(nn.Module):
    def __init__(self):
        super(UNet, self).__init__()

	#1/28/22: Incorporating Darin's suggestion from CT meeting abstract to use more filters
        self.conv1 = nn.Conv2d(4,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm1 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.conv1_2 = nn.Conv2d(32,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm1_2 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.pool1 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))


        self.conv2 = nn.Conv2d(32,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.conv2_2 = nn.Conv2d(64,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm2_2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.pool2 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))
        
        self.conv3 = nn.Conv2d(64,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm3 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.conv3_2 = nn.Conv2d(128,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm3_2 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.pool3 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))

        self.conv4 = nn.Conv2d(128,256,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm4 = nn.BatchNorm2d(num_features=256,dtype=torch.double)
        self.conv4_2 = nn.Conv2d(256,256,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm4_2 = nn.BatchNorm2d(num_features=256,dtype=torch.double)

        self.upsamp1 = nn.Upsample(scale_factor=2)
        self.conv5 = nn.Conv2d(384,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm5 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.conv5_2 = nn.Conv2d(128,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm5_2 = nn.BatchNorm2d(num_features=128,dtype=torch.double)

        self.upsamp2 = nn.Upsample(scale_factor=2)
        self.conv6 = nn.Conv2d(192,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm6 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.conv6_2 = nn.Conv2d(64,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm6_2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)

        self.upsamp3 = nn.Upsample(scale_factor=2)
        self.conv7 = nn.Conv2d(96,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm7 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.conv7_2 = nn.Conv2d(32,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm7_2 = nn.BatchNorm2d(num_features=32,dtype=torch.double)

        self.conv_out = nn.Conv2d(32,4,kernel_size=(1,1),stride=(1,1),padding='same',dtype=torch.double)


    def forward(self,x):
        #3/21/22: Incorporating Darin's suggestion to do 2x conv, batchnorm, leaky_relu at each level like other U-net papers
        #import ipdb; ipdb.set_trace() #python breakpoint library. Enter n to go to next line
        c1 = self.conv1(x)
        n1 = self.norm1(c1)
        r1 = F.leaky_relu(n1,negative_slope=0.2)
        c1_2 = self.conv1_2(r1)
        n1_2 = self.norm1_2(c1_2)
        r1_2 = F.leaky_relu(n1_2,negative_slope=0.2)
        p1 = self.pool1(r1_2)

        c2 = self.conv2(p1)
        n2 = self.norm2(c2)
        r2 = F.leaky_relu(n2,negative_slope=0.2)
        c2_2 = self.conv2_2(r2)
        n2_2 = self.norm2_2(c2_2)
        r2_2 = F.leaky_relu(n2_2,negative_slope=0.2)
        p2 = self.pool2(r2_2)

        c3 = self.conv3(p2)
        n3 = self.norm3(c3)
        r3 = F.leaky_relu(n3,negative_slope=0.2)
        c3_2 = self.conv3_2(r3)
        n3_2 = self.norm3_2(c3_2)
        r3_2 = F.leaky_relu(n3_2,negative_slope=0.2)
        p3 = self.pool3(r3_2)

        c4 = self.conv4(p3)
        n4 = self.norm4(c4)
        r4 = F.leaky_relu(n4,negative_slope=0.2)
        c4_2 = self.conv4_2(r4)
        n4_2 = self.norm4_2(c4_2)
        r4_2 = F.leaky_relu(n4_2,negative_slope=0.2)

        u1 = self.upsamp1(r4_2)
        co1 = torch.cat((r3_2,u1),dim=1)

        c5 = self.conv5(co1)
        n5 = self.norm5(c5)
        r5 = F.leaky_relu(n5,negative_slope=0.2)
        c5_2 = self.conv5_2(r5)
        n5_2 = self.norm5_2(c5_2)
        r5_2 = F.leaky_relu(n5_2,negative_slope=0.2)

        u2 = self.upsamp2(r5_2)

        co2 = torch.cat((r2_2,u2),dim=1)
        c6 = self.conv6(co2)
        n6 = self.norm6(c6)
        r6 = F.leaky_relu(n6,negative_slope=0.2)
        c6_2 = self.conv6_2(r6)
        n6_2 = self.norm6_2(c6_2)
        r6_2 = F.leaky_relu(n6_2,negative_slope=0.2)

        u3 = self.upsamp3(r6_2)

        co3 = torch.cat((r1_2,u3),dim=1)
        c7 = self.conv7(co3)
        n7 = self.norm7(c7)
        r7 = F.leaky_relu(n7,negative_slope=0.2)
        c7_2 = self.conv7_2(r7)
        n7_2 = self.norm7_2(c7_2)
        r7_2 = F.leaky_relu(n7_2,negative_slope=0.2)

        c_out = self.conv_out(r7_2)
        
        t_out = F.tanhshrink(c_out)

        return t_out


start_time = time.time()
print("Generating test set predictions")
model = UNet()

if torch.cuda.device_count() > 1:
    print("Let's use ",torch.cuda.device_count()," GPUs!")
    model = nn.DataParallel(model)

model = model.to(device)
model.load_state_dict(torch.load('models/model_UnetUEnergy.pth'))
print("Loaded pretrained model")

crop = False

filename_wfbp1 = 'data/RealMouseTestSet/wFBP/wFBP_3500projections_slices21to260.nii'

#Just start and end of volume values because we're not cropping the volume
crop_start_1 = 0
crop_end_1 = 240

nt = 10
ne = 4

pcd_wfbp1 = loadCardiacSet(filename_wfbp1,crop_start_1,crop_end_1,crop)

print("loaded wFBP NIFTI file to volume")

U0, Minv, Ug, S, Vh, Winv = makeNormalizedHPSingularVector(pcd_wfbp1)
print("Completed transformation to U domain")
U0 = torch.permute(U0,(2,3,0,1))
print("U0 size")
print(U0.size())    
Ug = torch.permute(torch.squeeze(Ug),(1,0,2,3))
print("Ug size")
print(Ug.size())
settest= torch.zeros(U0.size(dim=0))

#I/PE/CS sensitivity matrix
#You don't actually need sensitivity matrix at test time, you just
#add it to the CustomDataset to avoid errors
Md = 1000*torch.Tensor([[15*0.0002492362734,15*0.0002379894912,15*0.0001560676012,15*0.0001241109561],[2.5*0.0003290817964,2.5*0.0002350570711,2.5*0.0001121347128,2.5*7.736935683e-05],\
                     [0.002416914899,0.002372784439,0.002314651403,0.002292430294]]).double()


Minv = torch.unsqueeze(Minv,dim=0)
S = torch.unsqueeze(S,dim=0)
Vh = torch.unsqueeze(Vh,dim=0)
Winv = torch.unsqueeze(Winv,dim=0)
Md = torch.unsqueeze(Md,dim=0)

#in this case, I am simply plugging the input into the spot where the label is supposed to go
testDat = CustomDataset(U0,Ug,U0,U0,settest,Minv,S,Vh,Winv,Md,flips=False)
testLoader = DataLoader(testDat,batch_size=batch_size)

print("prepared data loader")

pred_iter = torch.zeros(U0.size())
pred_iter = pred_iter.to(device)

idx = 0
with torch.no_grad():
    for batch_idx,(U0b, Ugb, U0b2, U0b3, setsb, Minvb, Sb, Vhb, Winvb, Mdb) in enumerate(testLoader):
        print(idx)
        U0b, Ugb, U0b2, U0b3, setsb, Minvb, Sb, Vhb, Winvb, Mdb = U0b.to(device), Ugb.to(device), U0b2.to(device), U0b3.to(device), setsb.to(device), Minvb.to(device), Sb.to(device), Vhb.to(device), Winvb.to(device), Mdb.to(device)
        Uhatb = model(U0b)
        predictX = recoverPCDImagesFromUhat(Uhatb, Ugb, setsb, Minvb, Sb, Vhb, Winvb)

        if(idx+batch_size >= pred_iter.size(dim=0)):
            pred_iter[idx:,:,:,:] = predictX
        else:
            pred_iter[idx:idx+batch_size,:,:,:] = predictX

        del predictX,U0b,Ugb

        idx = idx+batch_size


pred_iter = pred_iter.detach().to('cpu').numpy()
pred_iter = pred_iter/1000 #reverse multiplication by 1000 in loadCardiacSet

pred_iter = np.transpose(pred_iter,(2,3,0,1))
pred_iter = np.reshape(pred_iter,(pred_iter.shape[0],pred_iter.shape[1],int(pred_iter.shape[2]/10),10,4))

saveimg = nib.Nifti1Image(pred_iter,np.eye(4))
savepath = 'data/RealMouseTestSet/wFBP_3500projections_denoised_UnetUEnergy.nii'
nib.save(saveimg,savepath)

print("Done. Total time:")
print(time.time() - start_time, " seconds")


