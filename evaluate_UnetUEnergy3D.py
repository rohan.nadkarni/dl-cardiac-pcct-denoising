#Written by: Rohan Nadkarni

#This code shows an example of how to use the trained UnetU Energy 3D model to denoise a
#5D (3D + energy + time) cardiac PCCT wFBP reconstruction of a mouse that was held out for testing.
#For more information about this data, see Section 2.2 of our manuscript. Our training procedures
#are described in Section 2.5 of our manuscript.

import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, TensorDataset, DataLoader
from torch.utils.tensorboard import SummaryWriter
import torchvision
import torchvision.transforms as T
import nibabel as nib
from common_functions_UnetUDenoising import loadCardiacSet, makeNormalizedHPSingularVector, create_circular_mask, make1DSeparableGaussianKernelsFor3DFilter, makeUhatLabelFromFDKMatrices  
from common_functions_UnetU3DDenoising import recoverPCDImagesFromUhat_3D, recoverDecompFromX_3D, MSEofXandUandCPlusMSEofXBlurred_3D, CustomTestDataset3D, CustomDataset3D_V2
from patch_functions import unpatch_3D, get_stencil_3D, patch, make3DPatchesFromU0andUg
import time

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
batch_size = 8

class UNet3D(nn.Module):
    def __init__(self):
        super(UNet3D, self).__init__()

        self.conv1 = nn.Conv3d(4,32,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm1 = nn.BatchNorm3d(num_features=32,dtype=torch.double)
        self.conv1_2 = nn.Conv3d(32,32,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm1_2 = nn.BatchNorm3d(num_features=32,dtype=torch.double)
        self.pool1 = nn.AvgPool3d(kernel_size=(2,2,2), stride=(2,2,2),padding=(0,0,0))


        self.conv2 = nn.Conv3d(32,64,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm2 = nn.BatchNorm3d(num_features=64,dtype=torch.double)
        self.conv2_2 = nn.Conv3d(64,64,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm2_2 = nn.BatchNorm3d(num_features=64,dtype=torch.double)
        self.pool2 = nn.AvgPool3d(kernel_size=(2,2,2), stride=(2,2,2),padding=(0,0,0))
        
        self.conv3 = nn.Conv3d(64,128,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm3 = nn.BatchNorm3d(num_features=128,dtype=torch.double)
        self.conv3_2 = nn.Conv3d(128,128,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)        
        self.norm3_2 = nn.BatchNorm3d(num_features=128,dtype=torch.double)
        self.pool3 = nn.AvgPool3d(kernel_size=(2,2,2), stride=(2,2,2),padding=(0,0,0))

        self.conv4 = nn.Conv3d(128,256,kernel_size=(3,3,3),stride=(1,1,1),padding=(1,1,1),dtype=torch.double)
        self.norm4 = nn.BatchNorm3d(num_features=256,dtype=torch.double)
        self.conv4_2 = nn.Conv3d(256,256,kernel_size=(3,3,3),stride=(1,1,1),padding=(1,1,1),dtype=torch.double)        
        self.norm4_2 = nn.BatchNorm3d(num_features=256,dtype=torch.double)

        self.upsamp1 = nn.Upsample(scale_factor=2)
        self.conv5 = nn.Conv3d(384,128,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm5 = nn.BatchNorm3d(num_features=128,dtype=torch.double)
        self.conv5_2 = nn.Conv3d(128,128,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)        
        self.norm5_2 = nn.BatchNorm3d(num_features=128,dtype=torch.double)

        self.upsamp2 = nn.Upsample(scale_factor=2)
        self.conv6 = nn.Conv3d(192,64,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm6 = nn.BatchNorm3d(num_features=64,dtype=torch.double)
        self.conv6_2 = nn.Conv3d(64,64,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm6_2 = nn.BatchNorm3d(num_features=64,dtype=torch.double)

        self.upsamp3 = nn.Upsample(scale_factor=2)
        self.conv7 = nn.Conv3d(96,32,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm7 = nn.BatchNorm3d(num_features=32,dtype=torch.double)
        self.conv7_2 = nn.Conv3d(32,32,kernel_size=(3,3,3),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)
        self.norm7_2 = nn.BatchNorm3d(num_features=32,dtype=torch.double)

        self.conv_out = nn.Conv3d(32,4,kernel_size=(1,1,1),stride=(1,1,1),padding=(0,0,0),dtype=torch.double)


    def forward(self,x):
        c1 = self.conv1(x)
        n1 = self.norm1(c1)
        r1 = F.leaky_relu(n1,negative_slope=0.2)
        c1_2 = self.conv1_2(r1)
        n1_2 = self.norm1_2(c1_2)
        r1_2 = F.leaky_relu(n1_2,negative_slope=0.2)
        p1 = self.pool1(r1_2)

        c2 = self.conv2(p1)
        n2 = self.norm2(c2)
        r2 = F.leaky_relu(n2,negative_slope=0.2)
        c2_2 = self.conv2_2(r2)
        n2_2 = self.norm2_2(c2_2)
        r2_2 = F.leaky_relu(n2_2,negative_slope=0.2)
        p2 = self.pool2(r2_2)

        c3 = self.conv3(p2)
        n3 = self.norm3(c3)
        r3 = F.leaky_relu(n3,negative_slope=0.2)
        c3_2 = self.conv3_2(r3)
        n3_2 = self.norm3_2(c3_2)
        r3_2 = F.leaky_relu(n3_2,negative_slope=0.2)
        p3 = self.pool3(r3_2)

        c4 = self.conv4(p3)
        n4 = self.norm4(c4)
        r4 = F.leaky_relu(n4,negative_slope=0.2)
        c4_2 = self.conv4_2(r4)
        n4_2 = self.norm4_2(c4_2)
        r4_2 = F.leaky_relu(n4_2,negative_slope=0.2)

        u1 = self.upsamp1(r4_2)
        co1 = torch.cat((r3_2,u1),dim=1)

        c5 = self.conv5(co1)
        n5 = self.norm5(c5)
        r5 = F.leaky_relu(n5,negative_slope=0.2)
        c5_2 = self.conv5_2(r5)
        n5_2 = self.norm5_2(c5_2)
        r5_2 = F.leaky_relu(n5_2,negative_slope=0.2)
        
        u2 = self.upsamp2(r5_2)

        co2 = torch.cat((r2_2[:,:,8:-8,8:-8,8:-8],u2),dim=1)
        c6 = self.conv6(co2)
        n6 = self.norm6(c6)
        r6 = F.leaky_relu(n6,negative_slope=0.2)
        c6_2 = self.conv6_2(r6)
        n6_2 = self.norm6_2(c6_2)
        r6_2 = F.leaky_relu(n6_2,negative_slope=0.2)
        
        u3 = self.upsamp3(r6_2)

        co3 = torch.cat((r1_2[:,:,24:-24,24:-24,24:-24],u3),dim=1)
        c7 = self.conv7(co3)
        n7 = self.norm7(c7)
        r7 = F.leaky_relu(n7,negative_slope=0.2)
        c7_2 = self.conv7_2(r7)
        n7_2 = self.norm7_2(c7_2)
        r7_2 = F.leaky_relu(n7_2,negative_slope=0.2)
        
        c_out = self.conv_out(r7_2)
        
        t_out = F.tanhshrink(c_out)

        return t_out


start_time = time.time()
print("Generating test set predictions")
model = UNet3D()

if torch.cuda.device_count() > 1:
    print("Let's use ",torch.cuda.device_count()," GPUs!")
    model = nn.DataParallel(model)

model = model.to(device)
model.load_state_dict(torch.load('models/model_UnetUEnergy3D.pth'))
print("Loaded pretrained model")

crop = False

filename_wfbp1 = 'data/RealMouseTestSet/wFBP/wFBP_3500projections_slices21to260.nii'

#Just give start and end slices because we're not cropping the volume
crop_start_1 = 0
crop_end_1 = 240

pcd_wfbp1 = loadCardiacSet(filename_wfbp1,crop_start_1,crop_end_1,crop)

nt = 10
ne = 4

psize_in = [92,108,108]
psize_out = [36,52,52]

print("loaded wFBP NIFTI file to volume")

U0_1, Minv_1, Ug_1, S_1, Vh_1, Winv_1 = makeNormalizedHPSingularVector(pcd_wfbp1)
U0_1_all_patch, Ug_1_all_patch, set1 = make3DPatchesFromU0andUg(U0_1,Ug_1,psize_in,psize_out,0)
print("Sizes of U0 and Ug after patching")
print(U0_1_all_patch.size())
print(Ug_1_all_patch.size())

U0_1_all_patch = U0_1_all_patch.double()
Ug_1_all_patch = Ug_1_all_patch.double()

Minv_1 = torch.unsqueeze(Minv_1,dim=0).double()
S_1 = torch.unsqueeze(S_1,dim=0).double()
Vh_1 = torch.unsqueeze(Vh_1,dim=0).double()
Winv_1 = torch.unsqueeze(Winv_1,dim=0).double()

testDat = CustomTestDataset3D(U0_1_all_patch,Ug_1_all_patch,set1,Minv_1,S_1,Vh_1,Winv_1)
testLoader = DataLoader(testDat,batch_size=batch_size)

print("prepared data loader")

pred_iter = torch.zeros(Ug_1_all_patch.size())
pred_iter = pred_iter.to(device)
idx = 0
with torch.no_grad():
    for batch_idx,(U0b, Ugb, setsb, Minvb, Sb, Vhb, Winvb) in enumerate(testLoader):
        print(idx)
        U0b, Ugb, setsb, Minvb, Sb, Vhb, Winvb = U0b.to(device), Ugb.to(device), setsb.to(device), Minvb.to(device), Sb.to(device), Vhb.to(device), Winvb.to(device)
        Uhatb = model(U0b)
        predictX = recoverPCDImagesFromUhat_3D(Uhatb, Ugb, setsb, Minvb, Sb, Vhb, Winvb)

        if(idx+batch_size >= pred_iter.size(dim=0)):
            pred_iter[idx:,:,:,:,:] = predictX
        else:
            pred_iter[idx:idx+batch_size,:,:,:,:] = predictX

        del predictX,U0b,Ugb

        idx = idx+batch_size

pred_iter = pred_iter.detach().to('cpu').numpy()
pred_iter = pred_iter/1000 #reverse multiplication by 1000 in loadCardiacSet
pred_iter = np.transpose(pred_iter,(0,2,3,4,1))
print(pred_iter.shape)
#Mouse 230926-10: 2400/36 = 67, 400/52 = 8
pred_iter = np.reshape(pred_iter,(67,8,8,psize_out[0],psize_out[1],psize_out[2],ne))
xydim = 400
zdim = crop_end_1 - crop_start_1
zdim_allt = nt*zdim
predictFull1 = unpatch_3D(np.squeeze(pred_iter[:,:,:,:,:,:,0]), (zdim_allt,xydim,xydim))
predictFull2 = unpatch_3D(np.squeeze(pred_iter[:,:,:,:,:,:,1]), (zdim_allt,xydim,xydim))
predictFull3 = unpatch_3D(np.squeeze(pred_iter[:,:,:,:,:,:,2]), (zdim_allt,xydim,xydim))
predictFull4 = unpatch_3D(np.squeeze(pred_iter[:,:,:,:,:,:,3]), (zdim_allt,xydim,xydim))    
predictFull = np.stack((predictFull1,predictFull2,predictFull3,predictFull4),axis=3)
print(predictFull.shape)
predict2 = np.transpose(predictFull,(1,2,0,3)) #so that it's visible in .nii
predict2 = np.reshape(predict2,(xydim,xydim,zdim,nt,ne))
saveimg = nib.Nifti1Image(predict2,np.eye(4))
savepath = 'data/RealMouseTestSet/wFBP_3500projections_denoised_UnetUEnergy3D.nii'
nib.save(saveimg,savepath)

print("Done. Total time:")
print(time.time() - start_time, " seconds")

