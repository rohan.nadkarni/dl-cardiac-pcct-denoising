#Written by: Rohan Nadkarni & Darin Clark

#Functions for patching and unpatching of data used in
#UnetU Energy 3D denoising.

import torch
import numpy as np


def unpatch_3D(P, sz):
    # Reconstruct X from 3D+ patches
    # P: nx/psz_x[0],ny/psz_x[1],nz/psz_x[2],psz_x[0],psz_x[1],psz_x[2]

    X_out = np.transpose(P,[0,3,1,4,2,5]) # kx,ky,kz,px,py,pz => kx,px,ky,py,kz,pz
    sh    = X_out.shape
    X_out = np.reshape(X_out,[sh[0]*sh[1],sh[2]*sh[3],sh[4]*sh[5]]) # kx,px,ky,py,kz,pz => nx,ny,nz

    # Crop back down to input volume size as needed
    X_out = X_out[0:sz[0],0:sz[1],0:sz[2]]

    return X_out


def get_stencil_3D(chunk_size,psz,stride):

    steps = ( (chunk_size[0] + stride[0] - 1) // stride[0],
              (chunk_size[1] + stride[1] - 1) // stride[1],
              (chunk_size[2] + stride[2] - 1) // stride[2] )

    npatch = steps[0]*steps[1]*steps[2]

    # Sometimes numpy returns incorrect integer values...so convert after creating the stencil
    stencil = np.zeros( [steps[0],steps[1],steps[2],psz[0],psz[1],psz[2]], np.float64 )

    pnum = 0

    stepx = -1

    for x in range(0,chunk_size[0],stride[0]):

        # Adjust indices such that CNN output is in phase with the original volume
        xrng = np.arange(x,x+psz[0]) - (psz[0]-stride[0])//2
        xrng = np.abs(xrng) # mirror negative values instead of wrapping around
        xrng[xrng >= chunk_size[0]] = (chunk_size[0]-1) - (xrng[xrng >= chunk_size[0]]%(chunk_size[0]-1)) # mirror end values
        xrng = np.reshape(xrng,[-1,1,1])

        stepx += 1

        stepy = -1

        for y in range(0,chunk_size[1],stride[1]):

            # Adjust indices such that CNN output is in phase with the original volume
            yrng = np.arange(y,y+psz[1]) - (psz[1]-stride[1])//2
            yrng = np.abs(yrng) # mirror negative values instead of wrapping around
            yrng[yrng >= chunk_size[1]] = (chunk_size[1]-1) - (yrng[yrng >= chunk_size[1]]%(chunk_size[1]-1)) # mirror end values
            yrng = np.reshape(yrng,[1,-1,1])

            stepy += 1

            stepz = -1

            for z in range(0,chunk_size[2],stride[2]):

                # Adjust indices such that CNN output is in phase with the original volume
                zrng = np.arange(z,z+psz[2]) - (psz[2]-stride[2])//2
                zrng = np.abs(zrng) # mirror negative values instead of wrapping around
                zrng[zrng >= chunk_size[2]] = (chunk_size[2]-1) - (zrng[zrng >= chunk_size[2]]%(chunk_size[2]-1)) # mirror end values
                zrng = np.reshape(zrng,[1,1,-1])

                stepz += 1

                stencil[stepx,stepy,stepz,...] = xrng * chunk_size[1] * chunk_size[2] + yrng * chunk_size[2] + zrng

                pnum += 1

    stencil = stencil.astype('int32')

    return stencil


def patch(X,stencil):
# Create 3D+ patches from X
# Input: nx, ny, nz

# Output: kx=nx/psz_x[0],ky=ny/psz_x[1],kz=nz/psz_x[2],px=psz_x[0],py=psz_x[1],pz=psz_x[2]

    return np.take(X,stencil)

def make3DPatchesFromIter(pcd_iter,Uhat_iter,psize_out):
    #Want dimension order nz, nx, ny, ne prior to creating 3D patches
    pcd_iter = torch.permute(pcd_iter,(0,2,3,1)).numpy()
    Uhat_iter = torch.permute(torch.squeeze(Uhat_iter),(0,2,3,1)).numpy()

    print("Sizes of pcd_iter and Uhat_iter before patching")
    print(pcd_iter.shape)
    print(Uhat_iter.shape)

    stencil = get_stencil_3D([pcd_iter.shape[0],pcd_iter.shape[1],pcd_iter.shape[2]],psize_out,psize_out)
    stencilg = get_stencil_3D([Uhat_iter.shape[0],Uhat_iter.shape[1],Uhat_iter.shape[2]],psize_out,psize_out)
    
    #make 3D patches of PCD Iter Recon, the network label
    pcd_iter_all_patche1 = patch(pcd_iter[:,:,:,0],stencil)
    pcd_iter_all_patche2 = patch(pcd_iter[:,:,:,1],stencil)
    pcd_iter_all_patche3 = patch(pcd_iter[:,:,:,2],stencil)
    pcd_iter_all_patche4 = patch(pcd_iter[:,:,:,3],stencil)
    del pcd_iter
    pcd_iter_all_patch = np.stack((pcd_iter_all_patche1,pcd_iter_all_patche2,pcd_iter_all_patche3,pcd_iter_all_patche4),axis=6)
    del pcd_iter_all_patche1,pcd_iter_all_patche2,pcd_iter_all_patche3,pcd_iter_all_patche4
    pcd_iter_all_patch = np.reshape(pcd_iter_all_patch,(pcd_iter_all_patch.shape[0]*pcd_iter_all_patch.shape[1]*pcd_iter_all_patch.shape[2],pcd_iter_all_patch.shape[3],pcd_iter_all_patch.shape[4],pcd_iter_all_patch.shape[5],pcd_iter_all_patch.shape[6]))
    pcd_iter_all_patch = torch.Tensor(pcd_iter_all_patch)
    pcd_iter_all_patch = torch.permute(pcd_iter_all_patch,(0,4,1,2,3))

    #make 3D patches of Uhat Iter, the network label in singular vector domain
    Uhat_iter_all_patche1 = patch(Uhat_iter[:,:,:,0],stencil)
    Uhat_iter_all_patche2 = patch(Uhat_iter[:,:,:,1],stencil)
    Uhat_iter_all_patche3 = patch(Uhat_iter[:,:,:,2],stencil)
    Uhat_iter_all_patche4 = patch(Uhat_iter[:,:,:,3],stencil)
    del Uhat_iter
    Uhat_iter_all_patch = np.stack((Uhat_iter_all_patche1,Uhat_iter_all_patche2,Uhat_iter_all_patche3,Uhat_iter_all_patche4),axis=6)
    del Uhat_iter_all_patche1,Uhat_iter_all_patche2,Uhat_iter_all_patche3,Uhat_iter_all_patche4
    Uhat_iter_all_patch = np.reshape(Uhat_iter_all_patch,(Uhat_iter_all_patch.shape[0]*Uhat_iter_all_patch.shape[1]*Uhat_iter_all_patch.shape[2],Uhat_iter_all_patch.shape[3],Uhat_iter_all_patch.shape[4],Uhat_iter_all_patch.shape[5],Uhat_iter_all_patch.shape[6]))
    Uhat_iter_all_patch = torch.Tensor(Uhat_iter_all_patch)
    Uhat_iter_all_patch = torch.permute(Uhat_iter_all_patch,(0,4,1,2,3))

    return pcd_iter_all_patch, Uhat_iter_all_patch


def make3DPatchesFromU0andUg(U0,Ug,psize_in,psize_out,setnum):
    #Want dimension order nz, nx, ny, ne prior to creating 3D patches
    U0 = torch.permute(U0,(2,0,1,3)).numpy()
    Ug = torch.permute(torch.squeeze(Ug),(1,2,3,0)).numpy()
    print("Sizes of U0 and Ug before patching")
    print(U0.shape)
    print(Ug.shape)

    stencil = get_stencil_3D([U0.shape[0],U0.shape[1],U0.shape[2]],psize_in,psize_out)
    stencilg = get_stencil_3D([Ug.shape[0],Ug.shape[1],Ug.shape[2]],psize_out,psize_out)
    
    #make 3D patches of U0, the network input
    U0_all_patche1 = patch(U0[:,:,:,0],stencil)
    U0_all_patche2 = patch(U0[:,:,:,1],stencil)
    U0_all_patche3 = patch(U0[:,:,:,2],stencil)
    U0_all_patche4 = patch(U0[:,:,:,3],stencil)
    del U0
    U0_all_patch = np.stack((U0_all_patche1,U0_all_patche2,U0_all_patche3,U0_all_patche4),axis=6)
    del U0_all_patche1,U0_all_patche2,U0_all_patche3,U0_all_patche4
##    print("input size")
##    print(U0_all_patch.shape)
    U0_all_patch = np.reshape(U0_all_patch,(U0_all_patch.shape[0]*U0_all_patch.shape[1]*U0_all_patch.shape[2],U0_all_patch.shape[3],U0_all_patch.shape[4],U0_all_patch.shape[5],U0_all_patch.shape[6]))
    U0_all_patch = torch.Tensor(U0_all_patch)
    U0_all_patch = torch.permute(U0_all_patch,(0,4,1,2,3))

    #make 3D patches of Ug, the blurred left singular vector
    Ug_all_patche1 = patch(Ug[:,:,:,0],stencilg)
    Ug_all_patche2 = patch(Ug[:,:,:,1],stencilg)
    Ug_all_patche3 = patch(Ug[:,:,:,2],stencilg)
    Ug_all_patche4 = patch(Ug[:,:,:,3],stencilg)
    del Ug
    Ug_all_patch = np.stack((Ug_all_patche1,Ug_all_patche2,Ug_all_patche3,Ug_all_patche4),axis=6)
    del Ug_all_patche1,Ug_all_patche2,Ug_all_patche3,Ug_all_patche4
    Ug_all_patch = np.reshape(Ug_all_patch,(Ug_all_patch.shape[0]*Ug_all_patch.shape[1]*Ug_all_patch.shape[2],Ug_all_patch.shape[3],Ug_all_patch.shape[4],Ug_all_patch.shape[5],Ug_all_patch.shape[6]))
    Ug_all_patch = torch.Tensor(Ug_all_patch)
    Ug_all_patch = torch.permute(Ug_all_patch,(0,4,1,2,3))
    npatch = U0_all_patch.size(dim=0)
    print("number of patches:")
    print(npatch)
    #create set labels of the appropriate length
    sets = setnum*torch.ones(npatch) #4/22/2024: Corrected bug in old UnetU3D code
    return U0_all_patch, Ug_all_patch, sets


